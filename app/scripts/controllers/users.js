'use strict';

/**
 * @ngdoc function
 * @name instabugApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the instabugApp
 */
angular.module('instabugApp')
  .controller('UsersCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
