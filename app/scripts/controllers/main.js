'use strict';

/**
 * @ngdoc function
 * @name instabugApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the instabugApp
 */
angular.module('instabugApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
