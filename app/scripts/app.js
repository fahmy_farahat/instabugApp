'use strict';

/**
 * @ngdoc overview
 * @name instabugApp
 * @description
 * # instabugApp
 *
 * Main module of the application.
 */
var modules = [
    'ui.router',
    'ngAnimate',
];

var instabugApp = angular.module('instabugApp', modules);


function config(stateProvider) {
    // - Redirect from '/home' to '/'.
    // - Redirect from '/about/' to '/about'.
    // - Redirect from '/users/{not_a_number}' to '/users/{valid-user-id}'
    // - Redirect from any invalid url to the home page '/'
    stateProvider
        .state({
            name:'home',
            url:'/',
            templateUrl: 'views/main.html'
        }).state('otherwise', { 
            url : '',
            templateUrl: 'views/main.html'
        });
}
instabugApp.config(['$stateProvider', config]);
